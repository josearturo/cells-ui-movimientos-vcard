import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-movimientos-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-theme-vcard';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
/**
This component ...

Example:

```html
<cells-ui-movimientos-vcard></cells-ui-movimientos-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiMovimientosVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-movimientos-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      items: {
        type: Array
      },
      itemsDisplay: {
        type: Array
      }
    };
  }

  // Initialize properties
  constructor() {
    super();    
    this.items = [];
    this.itemsDisplay = [];
    this.updateComplete.then(() => {
      this.getById('input-search').addEventListener('input', () => {
        let query = this.getById('input-search').value;
        if (query) {
          query = query.toUpperCase();
          this.itemsDisplay = this.items.filter((item) => {
            return (item.tipoMovimiento.nombre + item.creacion).toUpperCase().indexOf(query) >= 0;
          });
        } else {
          this.itemsDisplay = this.items;
        }
        this.requestUpdate();
      });
    });
  }

  async setItems(data) {
    data = data.map((item)=>{
        item.creacion = this.formatDate(item.creacion);
        return item;
    });
    this.items = data;
    this.itemsDisplay = data;
    await this.requestUpdate();
  }

  getColorMovimiento(item) {
    if(item.tipoMovimiento.codigo === this.ctts.keys.movOut){
      return 'negative';
    }else {
      return 'positive';
    }
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-movimientos-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "panel-top" >
        <input id = "input-search" class = "input" placeholder = "Buscar" />
      </div>
      <div class = "content-table-movimientos">
      <table class = "green">
        <thead> 
          <tr>
            <th>Tipo</th>
            <th>Cantidad</th>
            <th>Fecha</th>
          </tr>
        </thead>
        <tbody>
        ${this.itemsDisplay.map((item) => html`
            <tr class = "${this.getColorMovimiento(item)}">
              <td>${this.extract(item, 'tipoMovimiento.nombre', '')}</td>
              <td>${this.extract(item, 'cantidad', '')}</td>
              <td>${this.extract(item, 'creacion', '')}</td>
            </tr>
        `)}
          
       
        </tbody>
      </table>
      </div>
      ${this.itemsDisplay.length === 0 ? html` <div style = "width: 100%;
                                                        padding: 15px;
                                                        border: 1px solid #BDBDBD;
                                                        text-align: center;
                                                        color: #666;">Sin resultados que mostrar.</div>` : html``}
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiMovimientosVcard.is, CellsUiMovimientosVcard);
